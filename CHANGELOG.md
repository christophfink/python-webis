- **0.3.1** (2020-12-04):
    - removed AUR PKGBUILD from package

- **0.2.1** (2020-04-14):
    - bugfix: CI/CD script did not run on updated containers
    - improvement: use XDG_CACHE_HOME instead of XDG_CONFIG_HOME

- **0.2.0** (2018-01-23):
    - feature: SentimentIdentifier().identifySentiment accepts dict

- **0.1.8** (2018-01-22): 
    - all functions implemented
